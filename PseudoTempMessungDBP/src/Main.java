import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Main //implements KeyListener
{
//	private static int running = 0;
	
	public static void main (String[] args) 
	{
		int messurePoint = 0;
		double timeSec = System.currentTimeMillis() / 1000;
		
		MySQLConnector dbm = MySQLConnector.getInstance();
		
		System.out.println("starting Messurement... \n");
		
		CreateTemperture temp = CreateTemperture.getInstance();
		temp.setTemp(temp.round(temp.getTemp()));
		System.out.println("0sec | " + temp.getTemp() + "�C");
		
		while(true)
		{
			if(System.currentTimeMillis() / 1000 - timeSec >= 1)
			{
				System.out.print((int) (System.currentTimeMillis() / 1000 - timeSec) + (double) messurePoint + "sec | ");
				
				timeSec = System.currentTimeMillis() / 1000;
				messurePoint++;
				temp.setTemp(temp.round(temp.newTemp()));
				
				System.out.println(temp.getTemp() + "�C");
				
				dbm.insertData(temp.getTemp());
			}
		}
	}

/*	@Override
	public void keyPressed(KeyEvent e) 
	{
		if(e.getKeyCode() == KeyEvent.VK_ENTER)
		{
			running = 10;
		}
	}

	@Override
	public void keyReleased(KeyEvent arg0) {  }

	@Override
	public void keyTyped(KeyEvent arg0) {	}
	*/
}
