
public class CreateTemperture 
{
	
	private static CreateTemperture ct = null;
	static Randomizer rdm = new Randomizer();
	double temp;
	
	private CreateTemperture(double temperture)
	{
		setTemp(temperture);
		
	}
	public static CreateTemperture getInstance()
	{
		if (ct == null)
		{
			ct = new CreateTemperture(rdm.getDoubleFromZero(40.5));
		}
		return ct;
	}
	
	public double newTemp()
	{
		setTemp(rdm.getDouble(this.temp - 0.5, this.temp + 0.5));
		return temp;
	}
	public double round(double number)
	{
		number = number * 100;
		int rounded = (int) number;
		number = (double) rounded;
		number = number / 100;
		return number;
	}
	
	public double getTemp() 
	{
		return temp;
	}
	public void setTemp(double temp) 
	{
		this.temp = temp;
	}
	
}
