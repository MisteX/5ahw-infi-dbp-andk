import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.swing.text.DateFormatter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.Date;

public class MySQLConnector 
{
	private static Connection c;
	private static MySQLConnector db = null;
	private static Statement stmt;
	
	private MySQLConnector()
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			c = DriverManager.getConnection("jdbc:mysql://localhost:3306/TempMessurement?useSSL=false","root","123");
			System.out.println("Connection succeeded!");
			
			createTable();
		}
		catch (ClassNotFoundException cnfe)
		{
			cnfe.printStackTrace();
		}
		catch (SQLException se)
		{
			se.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}

	public static MySQLConnector getInstance()
	{
		if(db == null) db = new MySQLConnector();

		return db;
	}

	public void createTable() throws IOException
	{
		String input;

		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);

		try
		{
			String sql = "CREATE TABLE TempMessure"
					+ "("
					+ "id INTEGER AUTO_INCREMENT,"
					+ "time DATETIME,"
					+ "temperture REAL NOT NULL,"
					+ "PRIMARY KEY(id)"
					+ ");";

			PreparedStatement cr = c.prepareStatement(sql);
			cr.executeUpdate();
			cr.close();
		}
		catch (SQLException se)
		{
			System.out.println("Table is already existing! Drop it? [y/n]");
			input = br.readLine();
			if(input.equals("y")) 
			{
				dropTable();
				createTable();
			}
			
			if(input.equals("n")) 
			{
				return;
			}
		
		}
	}

	public void dropTable()
	{
		String sql = "drop table TempMessure;";
		try 
		{
			stmt = c.createStatement();
			stmt.executeUpdate(sql);
			stmt.close();

			System.out.println("dropping succeeded!");
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}

	}

	public void insertData(double temp)
	{
//		String d = "30-09-2018";
//		java.util.Date date;
//		try 
//		{
//			date = new SimpleDateFormat("dd-MM-yyyy").parse(d);
//		} catch (ParseException e) 
//		{
//			e.printStackTrace();
//			return;
//		}
//		String Date = "\\d\\d\\d\\d\\D[0-1][0-9]\\D[0-3][0-9]";
//		String date = DateMatcher.group().trim();
		
//		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
//		Date myDate;
//		try 
//		{
//			myDate = (Date) formatter.parse("2018-09-30");
//		} 
//		catch (ParseException e) 
//		{
//			e.printStackTrace();
//			return;
//		}
		
//		java.sql.Date sqlDate = new java.sql.Date(myDate.getTime());
//		java.sql.Timestamp sqlTime = new java.sql.Timestamp(date.getTime()); 
		
		try
		{
			String sql = "INSERT INTO TempMessure(temperture) VALUES "
					+ "("
//					+ sqlDate
//					+ ", "
					+ temp
					+ ");";
			
			PreparedStatement ins = c.prepareStatement(sql);
			
			
			ins.executeUpdate();
			
			ins.close();

		}
		catch (SQLException se)
		{
			se.printStackTrace();
		}
	}
}

