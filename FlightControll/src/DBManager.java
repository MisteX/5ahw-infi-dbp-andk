import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DBManager 
{
	private static Connection c;
	private static DBManager db = null;
	private static PreparedStatement stmt;
	private static int messureLength;
	private static ResultSet rs = null;

	private DBManager()
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			c = DriverManager.getConnection("jdbc:mysql://localhost:3306/aircraft?useSSL=false","root","123");
			System.out.println("Connection succeeded!");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public static DBManager getInstance()
	{
		if(db == null) db = new DBManager();

		return db;
	}

	public ArrayList<Integer> getFlightDate(Date date) throws SQLException
	{
		ArrayList<Integer> months = new ArrayList<Integer>();
		long stampUp = 0;
		long stampDown = 0;
		/*
		Calendar cal = Calendar.getInstance();
		date = cal.getTime();

		int month = cal.get(Calendar.MONTH) + 1;
		int year = cal.get(Calendar.YEAR);

		long stampUp = (long) date.getTime() / 1000;

		cal.set(Calendar.DAY_OF_MONTH, 1);

		date = cal.getTime();

		long stampDown = (long) date.getTime() / 1000;
		int monthlength;

		for (int j = 0; j < 13; j++) 
		{
			if ((month == 1) || (month == 3) || (month == 5) || (month == 7) || (month == 8) || (month == 10) || (month == 12)) 
			{
				monthlength = 31;
			} 
			else if ((month == 2) && (year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0))) 
			{
				monthlength = 29;
			}
			else if ((month == 2) && (year % 4 != 0)) 
			{
				monthlength = 28;
			} 
			else 
			{
				monthlength = 30;
			}

			if(j != 0)
			{
				stampDown = stampDown - 1 * monthlength * 24 * 60 * 60;
			}

			cal.setTimeInMillis(stampDown * 1000);
		 */			
		LocalDateTime now = LocalDate.now().atTime(0, 0, 0);
		LocalDateTime firstOfMonth = now.withDayOfMonth(1);

		//		System.out.println(firstOfMonth);

		ZoneId zoneId = ZoneId.systemDefault();

		for(int i = 0; i < 12; i++)
		{
			LocalDateTime monthBefore = firstOfMonth.minusMonths(i);

			//			System.out.println(monthBefore+ " : " + epoch);
			if(i == 0)
			{
				stampUp = now.atZone(zoneId).toEpochSecond();
				stampDown = firstOfMonth.atZone(zoneId).toEpochSecond();
			}
			else
			{
				stampUp = stampDown;
				stampDown = monthBefore.atZone(zoneId).toEpochSecond();
			}

			String sql = "SELECT count(DISTINCT flightnr) FROM dump1090data where seentime <= ? AND seentime >= ?;";
			stmt = c.prepareStatement(sql);
			stmt.setLong(1, stampUp);
			stmt.setLong(2, stampDown);
			rs = stmt.executeQuery();

			while(rs.next())
			{
				months.add(rs.getInt(1));
			}

		}

		if(rs != null) rs.close();
		if(stmt != null) stmt.close();

		return months;
	}

	public int getAmountHolidays() throws SQLException
	{
		String sql = "SELECT count(day) FROM holidays";
		int amount = 0;

		stmt = c.prepareStatement(sql);
		rs = stmt.executeQuery(sql); 
		while(rs.next())
		{
			amount = rs.getInt(1);
		}

		if(rs != null) rs.close();
		if(stmt != null) stmt.close();

		return amount;
	}

	public ArrayList<String> getHoliNames() throws SQLException
	{
		ArrayList<String> holyNames = new ArrayList<String>();
		String sql = "SELECT name FROM holidays";

		stmt = c.prepareStatement(sql);
		rs = stmt.executeQuery(sql); 
		while(rs.next())
		{
			holyNames.add(rs.getString(1));
		}

		if(rs != null) rs.close();
		if(stmt != null) stmt.close();

		return holyNames;
	}

	public long getHoliDate(int dbID) throws SQLException
	{
		Date date = new Date();
		long holidate;
		String sql = "SELECT day FROM holidays;";

		stmt = c.prepareStatement(sql);
		rs = stmt.executeQuery(sql); 

		for(int i = 0; i <= dbID; i++)
		{
			date = rs.getDate(1); //fehler!!!
		}
		holidate = date.getTime() / 1000;

		if(rs != null) rs.close();
		if(stmt != null) stmt.close();

		return holidate;
	}

	public int getAmountFlights(long stampUp, long stampDown) throws SQLException
	{
		String sql = "SELECT count(DISTINCT flightnr) FROM dump1090data where seentime <= ? AND seentime >= ?;";
		int amount = 0;

		stmt = c.prepareStatement(sql);
		stmt.setLong(1, stampUp);
		stmt.setLong(2, stampDown);

		rs = stmt.executeQuery(); 
		amount = rs.getInt(1);

		if(rs != null) rs.close();
		if(stmt != null) stmt.close();

		return amount;
	}

	public int getFirstDate(String s)
	{
		if((s.toLowerCase().equals("year"))||(s.toLowerCase().equals("jahr")))
		{
			LocalDateTime monthBefore = LocalDate.now().atTime(0, 0, 0).withDayOfMonth(1).minusMonths(11);
			int year = monthBefore.atZone(ZoneId.systemDefault()).getYear();

			return year;
		}
		if((s.toLowerCase().equals("month"))||(s.toLowerCase().equals("monat")))
		{
			LocalDateTime monthBefore = LocalDate.now().atTime(0, 0, 0).withDayOfMonth(1).minusMonths(11);
			int month = monthBefore.atZone(ZoneId.systemDefault()).getMonthValue();

			return month;
		}
		return -1;
	}
}