import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.TextAnchor;

public class Main 
{
	public static void main (String[] args)
	{
		Date date = new Date();
		DBManager dbm = DBManager.getInstance();
		ArrayList<Integer> dates = new ArrayList<Integer>();
		
		String stringMonth;
		
		try 
		{
			dates = dbm.getFlightDate(date);
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		int year = dbm.getFirstDate("year");
		int month = dbm.getFirstDate("month");
		
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		
		for(int i = dates.size() - 1; i >= 0; i--)
		{
			
//			System.out.println(dates.get(i));
			
			switch(month)
			{
				case 1: stringMonth = "Jan " + year; break;
				case 2: stringMonth = "Feb " + year; break;
				case 3: stringMonth = "Mar " + year; break;
				case 4: stringMonth = "Apr " + year; break;
				case 5: stringMonth = "May " + year; break;
				case 6: stringMonth = "Jun " + year; break;
				case 7: stringMonth = "Jul " + year; break;
				case 8: stringMonth = "Aug " + year; break;
				case 9: stringMonth = "Sep " + year; break;
				case 10: stringMonth = "Oct " + year; break;
				case 11: stringMonth = "Nov " + year; break;
				case 12: stringMonth = "Dec " + year; year++; break;
				default: stringMonth = "not decl"; break;
			}
			
			dataset.addValue(dates.get(i), stringMonth, stringMonth);
			

			month++;
			
			if(month > 12)
			{
				month = 1;
			}
			if(month <= 0)
			{
				month = 12;
			}
		}

		JFreeChart chart = ChartFactory.createBarChart("Numbers of flights of a year", // chart title
				"Months", // domain axis label
				"Numbers of Flights", // range axis label
				dataset, // data
				PlotOrientation.VERTICAL, // orientation
				true, // include legend
				true, // tooltips
				false // URLs
		);

		chart.getPlot().setBackgroundPaint(Color.WHITE);

//		Barrenderer renderer = (Barrenderer) chart.getCategoryPlot().getRenderer();
//		renderer.setItemMargin(-5);
//		renderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
//		renderer.setBaseItemLabelsVisible(true);
//		renderer.setItemLabelFont(new Font("Bernard MT Condensed", 8, 20));
//		ItemLabelPosition position = new ItemLabelPosition(ItemLabelAnchor.OUTSIDE12, TextAnchor.BASELINE_CENTER);
//		renderer.setBasePositiveItemLabelPosition(position);

		ChartPanel chartPanel = new ChartPanel(chart, false);
		chartPanel.setPreferredSize(new Dimension(1000, 600));

		ApplicationFrame punkteframe = new ApplicationFrame("Aircraft");

		punkteframe.setContentPane(chartPanel);
		punkteframe.pack();
		punkteframe.setVisible(true);
		
//		try 
//		{
//			long stampUp;
//			long stampDown;
//			String dateUp;
//			
//			
//			
//			System.out.println("holiday : total flights");
//			
//			for(int i = 0; i < dbm.getAmountHolidays(); i++)
//			{
//					System.out.print(dbm.getHoliNames().get(i) + " : ");
//					stampDown = dbm.getHoliDate(i);
//					
//					stampUp = stampDown + 24 * 60 * 60;
//					
//					System.out.println(dbm.getAmountFlights(stampUp, stampDown));
//			}
//		} 
//		catch (SQLException e) 
//		{
//			e.printStackTrace();
//		}
	}
}
